import { request } from '../config/axios';
import { IOrder } from '../modules/ShopPage/model/IShopState';

export const getOrders = (data: { shopId: string; start: number; limit: number }) => {
	const endpoint = `/shops/${data.shopId}/orders?start=${data.start}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const getDeliveryCountries = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}/delivery_countries`;
	return request(endpoint, 'GET', null);
};

export const getShippingCarrierByLocale = (data: { shopId: string; locale: string }) => {
	const endpoint = `/shops/${data.shopId}/shipping_carriers?locale=${data.locale}`;
	return request(endpoint, 'GET', null);
};

export const getFullFillOrders = (data: { shopId: string; limit: number; start: number }) => {
	const endpoint = `/shops/${data.shopId}/fullfill_orders?start=${data.start}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const postFullFillOrder = (data: { shopId: string; order: IOrder }) => {
	const endpoint = `/shops/${data.shopId}/fullfill_orders`;
	return request(endpoint, 'POST', { ...data.order });
};
