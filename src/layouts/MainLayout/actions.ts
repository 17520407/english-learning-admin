import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export const handleLogout = (): IActions.IHandleLogout => {
	return {
		type: Keys.HANDLE_LOGOUT,
	};
};

export const toggleModal = (data: { type: MAIN_LAYOUT_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			type: data.type,
		},
	};
};

//#region Admin Change User Password Actions
export const adminChangeUserPassword = (data: {
	email: string;
	password: string;
}): IActions.IAdminChangeUserPassword => {
	return {
		type: Keys.ADMIN_CHANGE_USER_PASSWORD,
		payload: data,
	};
};

export const adminChangeUserPasswordSuccess = (): IActions.IAdminChangeUserPasswordSuccess => {
	return {
		type: Keys.ADMIN_CHANGE_USER_PASSWORD_SUCCESS,
	};
};

export const adminChangeUserPasswordFail = (res: any): IActions.IAdminChangeUserPasswordFail => {
	return {
		type: Keys.ADMIN_CHANGE_USER_PASSWORD_FAIL,
		payload: res,
	};
};
//#endregion
