import React from 'react';
import { Input, Button, Typography, Modal, Form, Divider } from 'antd';
import { UserOutlined, LockOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { IMainLayoutProps } from '../../model/IMainLayoutProps';
import { MAIN_LAYOUT_MODAL } from '../../model/IMainLayoutState';

const { Title } = Typography;

interface IProps extends IMainLayoutProps {}

export const ChangeUserPassword: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing, toggleModalChangeUserPassword } = props.store.MainLayout;

	const onFinish = (data: { email: string; password: string }) => {
		props.actions.adminChangeUserPassword({ email: data.email, password: data.password });
	};
	return (
		<Modal
			visible={toggleModalChangeUserPassword}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: MAIN_LAYOUT_MODAL.CHANGE_USER_PASSWORD,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Title level={4}>Change User Password</Title>
				<Form.Item
					label="Email"
					name="email"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
						{
							type: 'email',
						},
					]}
					children={
						<Input
							placeholder="Enter email"
							prefix={<UserOutlined />}
							allowClear={true}
							disabled={isProcessing}
						/>
					}
				/>
				<Form.Item
					label="Password"
					name="password"
					dependencies={['confirm']}
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
						{
							min: 6,
						},
					]}
					children={
						<Input.Password
							placeholder="Enter password"
							prefix={<LockOutlined />}
							iconRender={(visible) =>
								visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
							}
							disabled={isProcessing}
						/>
					}
				/>
				<Form.Item
					label="Confirm Password"
					name="confirm"
					dependencies={['password']}
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						({ getFieldValue }) => ({
							validator(rule, value) {
								if (!value || getFieldValue('password') === value) {
									return Promise.resolve();
								}
								return Promise.reject(
									'The two passwords that you entered do not match!'
								);
							},
						}),
					]}
				>
					<Input.Password
						prefix={<LockOutlined />}
						placeholder="Enter confirm password"
						disabled={isProcessing}
					/>
				</Form.Item>
				<Divider />

				<Button
					block={true}
					className="text-capitalize"
					type="primary"
					htmlType="submit"
					loading={isProcessing}
				>
					Update
				</Button>
			</Form>
		</Modal>
	);
};
