import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IMainLayoutState, initialState, MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export const name = 'MainLayout';

export const reducer: Reducer<IMainLayoutState, any> = (
	state: IMainLayoutState = initialState,
	action: ActionTypes
): IMainLayoutState => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);
		case Keys.HANDLE_LOGOUT:
			return onHandleLogout(state, action);

		case Keys.ADMIN_CHANGE_USER_PASSWORD:
			return onAdminChangeUserPassword(state, action);
		case Keys.ADMIN_CHANGE_USER_PASSWORD_SUCCESS:
			return onAdminChangeUserPasswordSuccess(state, action);
		case Keys.ADMIN_CHANGE_USER_PASSWORD_FAIL:
			return onAdminChangeUserPasswordFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: IMainLayoutState, action: IActions.IToggleModal) => {
	const { type } = action.payload;

	switch (type) {
		case MAIN_LAYOUT_MODAL.CHANGE_USER_PASSWORD:
			return {
				...state,
				toggleModalChangeUserPassword: !state.toggleModalChangeUserPassword,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleLogout = (state: IMainLayoutState, action: IActions.IHandleLogout) => {
	return {
		...state,
	};
};

const onAdminChangeUserPassword = (
	state: IMainLayoutState,
	action: IActions.IAdminChangeUserPassword
) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onAdminChangeUserPasswordSuccess = (
	state: IMainLayoutState,
	action: IActions.IAdminChangeUserPasswordSuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalChangeUserPassword: false,
	};
};
const onAdminChangeUserPasswordFail = (
	state: IMainLayoutState,
	action: IActions.IAdminChangeUserPasswordFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};
