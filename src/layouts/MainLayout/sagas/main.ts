import { takeEvery, call, put, delay } from 'redux-saga/effects';
import { message } from 'antd';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';

export function* handleAdminChangeUserPassword(action: any) {
	try {
		const res = yield call(AuthApi.adminChangePassword, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Change User Password Successful', 2);
			yield put(actions.adminChangeUserPasswordSuccess());
		} else {
			const { details } = res.data;
			if (details.length > 0) {
				message.error(details[0].message, 2);
				// yield put(actions.adminChangeUserPasswordFail(res.data));
			}
		}
	} catch (error) {
		yield put(actions.adminChangeUserPasswordFail(error));
	}
}

// eslint-disable-next-line
function* handleLogoutAccount() {
	localStorage.clear();
	window.location.reload();
}
/*-----------------------------------------------------------------*/
function* watchLogoutAccount() {
	yield takeEvery(Keys.HANDLE_LOGOUT, handleLogoutAccount);
}
function* watchAdminChangeUserPassword() {
	yield takeEvery(Keys.ADMIN_CHANGE_USER_PASSWORD, handleAdminChangeUserPassword);
}
/*-----------------------------------------------------------------*/
const sagas = [watchLogoutAccount, watchAdminChangeUserPassword];
export default sagas;
