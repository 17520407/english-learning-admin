import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { ILogInState, initialState } from './model/ILoginState';

export const name = 'LoginPage';

export const reducer: Reducer<ILogInState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.USER_LOGIN:
			return onUserLogin(state, action);
		case Keys.USER_LOGIN_SUCCESS:
			return onUserLoginSuccess(state, action);
		case Keys.USER_LOGIN_FAIL:
			return onUserLoginFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onUserLogin = (state: ILogInState, action: IActions.IUserLogin) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUserLoginSuccess = (state: ILogInState, action: IActions.IUserLoginSuccess) => {
	const { token, role } = action.payload;
	return {
		...state,
		accessToken: token,
		role,
		isProcessing: false,
	};
};
const onUserLoginFail = (state: ILogInState, action: IActions.IUserLoginFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
