import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';

// Handle User Login
function* handleUserLogin(action: any) {
	try {
		const res = yield call(AuthApi.login, action.payload);
		yield delay(500);
		if (res.status === 200) {
			const { token, role } = res.data;
			if (role !== undefined) {
				localStorage.setItem('role', role);
			}
			localStorage.setItem('accessToken', token);
			message.success('Login Successful', 1);
			yield put(actions.userLoginSuccess(res.data));
		} else {
			const { error } = res.data;
			message.error(error.message, 3);
			yield put(actions.userLoginFail(res));
		}
	} catch (error) {
		message.error('Server Error');
		yield put(actions.userLoginFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchUserLogin() {
	yield takeEvery(Keys.USER_LOGIN, handleUserLogin);
}

/*-----------------------------------------------------------------*/
const sagas = [watchUserLogin];
export default sagas;
