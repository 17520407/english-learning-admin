import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IRegisterState, initialState } from './model/IRegisterState';

export const name = 'RegisterPage';

export const reducer: Reducer<IRegisterState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.USER_REGISTER:
			return onUserRegister(state, action);
		case Keys.USER_REGISTER_SUCCESS:
			return onUserRegisterSuccess(state, action);
		case Keys.USER_REGISTER_FAIL:
			return onUserRegisterFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onUserRegister = (state: IRegisterState, action: IActions.IUserRegister) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUserRegisterSuccess = (state: IRegisterState, action: IActions.IUserRegisterSuccess) => {
	// const { accessToken, refreshToken, role } = action.payload.data;
	// localStorage.setItem('roleName', role[0].roleName);

	return {
		...state,
		isProcessing: false,
	};
};
const onUserRegisterFail = (state: IRegisterState, action: IActions.IUserRegisterFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
