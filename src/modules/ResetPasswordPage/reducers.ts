import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IResetPasswordState, initialState } from './model/IResetPasswordState';

export const name = 'ResetPasswordPage';

export const reducer: Reducer<IResetPasswordState, ActionTypes> = (
	state = initialState,
	action
) => {
	switch (action.type) {
		case Keys.USER_RESET_PASSWORD:
			return onUserResetPassword(state, action);
		case Keys.USER_RESET_PASSWORD_SUCCESS:
			return onUserResetPasswordSuccess(state, action);
		case Keys.USER_RESET_PASSWORD_FAIL:
			return onUserResetPasswordFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onUserResetPassword = (state: IResetPasswordState, action: IActions.IUserResetPassword) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUserResetPasswordSuccess = (
	state: IResetPasswordState,
	action: IActions.IUserResetPasswordSuccess
) => {
	// const { accessToken, refreshToken, role } = action.payload.data;
	// localStorage.setItem('roleName', role[0].roleName);

	return {
		...state,
		isProcessing: false,
	};
};
const onUserResetPasswordFail = (
	state: IResetPasswordState,
	action: IActions.IUserResetPasswordFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};
