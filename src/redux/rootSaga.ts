/**
 * @file root sagas
 */

import { all } from 'redux-saga/effects';

// Place for sagas' app
import { sagas as LoginSaga } from '../modules/LoginPage';
import { sagas as RegisterSaga } from '../modules/RegisterPage';
import { sagas as MainLayout } from '../layouts/MainLayout';

/*----Sagas List-----------------*/
export default function* rootSaga() {
	yield all([RegisterSaga(), LoginSaga(), MainLayout()]);
}
