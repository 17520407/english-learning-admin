import React, { ComponentClass, FunctionComponent, LazyExoticComponent } from 'react';
import { GetCodePage } from '../components';
import { routeName } from './routes-name';

const LoginPage = React.lazy(() => import('../modules/LoginPage/components/LoginPageContainer'));
const ResetPasswordPage = React.lazy(
	() => import('../modules/ResetPasswordPage/components/ResetPasswordPageContainer')
);
const RegisterPage = React.lazy(
	() => import('../modules/RegisterPage/components/RegisterPageContainer')
);

export interface RouteConfig {
	path: string;
	extract: boolean;
	component: ComponentClass | FunctionComponent | LazyExoticComponent<any>;
	permission?: string;
}

export const authRoutes: RouteConfig[] = [
	{
		path: routeName.login,
		extract: true,
		component: LoginPage,
	},
	{
		path: routeName.register,
		extract: true,
		component: RegisterPage,
	},
	{
		path: routeName.resetPassword,
		extract: true,
		component: ResetPasswordPage,
	},
];

export const mainRoutes: RouteConfig[] = [
	{
		path: routeName.shopCode,
		extract: true,
		component: GetCodePage,
	},
];
