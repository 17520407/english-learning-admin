export const routeName = {
	login: '/login',
	register: '/register',
	resetPassword: '/reset-password',
	shop: '/shop',
	order: '/order',
	shopCode: '/shop-code',
	notAccept: '/not-accept',
};
